const 
    axios = require('axios'),
    env = require('dotenv');


env.config();

const server = process.env.BASEURL+":8001/api/v1?fn=";

module.exports = (fn, data, token) =>{
    return new Promise((resolve, reject)=>{
        let options = {};
        if(token){
            options = {
                method : 'POST',
                headers : {
                    'Token' : token
                },
                url : `${server+fn}`,
                data : data
            }
        } else {
            options = {
                method : 'POST',
                url : `${server+fn}`,
                data : data
            }
        }
        
        axios(options)
            .then((res)=>{
                resolve(res)
            })
    })
}