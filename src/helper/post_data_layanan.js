const 
    axios = require('axios'),
    env = require('dotenv');

env.config();

const server = process.env.BASEURL;

module.exports = (port,fn,data,token) =>{
    return new Promise((resolve, reject)=>{
        let options = {};
        if(token){
            options = {
                method : "POST",
                headers : {
                    'token' : token
                },
                url : `${server}:${port}/api/v1?fn=${fn}`,
                data : data
            }
        } else {
            options = {
                method : "POST",
                url : `${server}:${port}/api/v1?fn=${fn}`,
                data : data
            }
        }

        axios(options)
            .then((res)=>{
                resolve(res)
            })
            .catch((err)=>{
                reject(err)
            })
    })
}