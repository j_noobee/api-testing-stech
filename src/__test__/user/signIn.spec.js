const post_data = require('../../helper/post_data');

describe('/api/v1?fn=signIn POST',()=>{
    const signin = {
        username : "senang",
        password : "senang1903"
    };

    const fn = 'signIn';

    it('Status Code harus 200',async(done)=>{
        const res = await post_data(fn,signin);
        expect(res.status).toBe(200);
        done();
    })

    it('Memiliki data yang di return', async(done)=>{
        const res = await post_data(fn,signin);
        let empty = true;
        if(res.data.data){
            empty = false 
        }
        expect(empty).toBeFalsy();
        done();
    })

    it('Mereturn Token', async(done)=>{
        const res = await post_data(fn,signin);
        let token = false;
        if(res.data.data.token){
            token = true 
        }
        expect(token).toBeTruthy();
        done();
    })
})