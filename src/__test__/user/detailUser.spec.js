const post_data = require('../../helper/post_data');


describe('/api/v1?fn=detailUser POST', ()=>{
    const signin = {
        username : "senang",
        password : "senang1903"
    };

    const fn1 = 'signIn';
    const fn2 = 'detailUser';
    it('Status Code harus 200', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        expect(res2.status).toBe(200);
        done()

    })

    it('Memiliki data yang di return', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        let empty = true;
        if(res.data.data){
            empty = false;
        }

        expect(empty).toBeFalsy();
        done()
    })

    it('Memiliki return ID', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        const id = res2.data.data.id;
        expect(id).not.toBeNull();
        done()
    })

    it('Permission harus dalam array', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        const permission = res2.data.data.permission;
        const isArray = true;
        if(permission.length){
            if(Array.isArray(permission) === false) isArray = false;
        } 
        else{
            isArray = false;
        }
        expect(isArray).toBeTruthy();
        done()
    })
})