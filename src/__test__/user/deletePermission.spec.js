const post_data = require('../../helper/post_data');

beforeEach(async()=>{
    const signin = {
        username : "senang",
        password : "senang1903"
    };

    const fn1 = 'signIn';
    const fn2 = 'detailUser';
    const fn3 = 'updatePermission';

    const permission = ['findUserExample'];

    const res = await post_data(fn1, signin);
    const token = res.data.data.token;

    const res2 = await post_data(fn2, signin, token);
    const id = res2.data.data.id;
    
    const data = {
        id : id,
        permission : permission
    };

    await post_data(fn3, data, token);
})

describe('/api/v1?fn=deletePermission POST',()=>{
    const signin = {
        username : "senang",
        password : "senang1903"
    };

    const fn1 = 'signIn';
    const fn2 = 'detailUser';
    const fn3 = 'deletePermission';

    const permission = "findUserExample";

    it('Status Code harus 200', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        const id = res2.data.data.id;
        
        const data = {
            id : id,
            permission : permission
        };

        const res3 = await post_data(fn3, data, token);
        expect(res3.status).toBe(200);
        done();
    })

    it('Memiliki data yg di return', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        const id = res2.data.data.id;
        
        const data = {
            id : id,
            permission : permission
        };

        const res3 = await post_data(fn3, data, token);
        const data3 = res3.data;
        let empty = true;
        if(data3.data){
            empty = false;
        }
        expect(empty).toBeFalsy()
        done();
    })

    it('Total permission baru < dari permission lama', async(done)=>{
        const res = await post_data(fn1, signin);
        const token = res.data.data.token;

        const res2 = await post_data(fn2, signin, token);
        const id = res2.data.data.id;
        
        const data = {
            id : id,
            permission : permission
        };

        const data2 = res2.data.data.permission;

        const res3 = await post_data(fn3, data, token);
        const data3 = res3.data.data.permission;

        let isChange = false;
        if(data2.length > data3.length){
            isChange = true;
        }
        expect(isChange).toBeTruthy();
        done();
    })
})