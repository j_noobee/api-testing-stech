const post_data = require('../../helper/post_data');

describe('/api/v1?fn=registerNewUser', () => {
    const fn = 'registerNewUser'
    it('Status Code harus 200', async(done)=>{
        const rand = btoa(new Date().getTime()).split("=")[0].toLowerCase();
        const count = parseInt(rand.length);
        const id = rand.slice(count-5, count);

        const user = {
            username : `usertest_${id}`,
            password : `usertest`,
            email    : `usertest${id}@gmail.com`
        }

        const res = await post_data(fn, user);
        const status = res.status;

        expect(status).toBe(200);
        done()
    })
    
    it('Memiliki data yang di return', async(done)=>{
        const rand = btoa(new Date().getTime()).split("=")[0].toLowerCase();
        const count = parseInt(rand.length);
        const id = rand.slice(count-5, count);

        const user = {
            username : `usertest_${id}`,
            password : `usertest`,
            email    : `usertest${id}@gmail.com`
        }
        
        let isEmpty = true;
        const res = await post_data(fn, user);
        if(res.data.data){
            isEmpty = false;
        }

        expect(isEmpty).toBeFalsy();
        done()
    })
});
