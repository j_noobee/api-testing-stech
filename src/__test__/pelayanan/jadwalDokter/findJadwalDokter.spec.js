const 
    post_data = require('../../../helper/post_data'),
    post_layanan = require('../../../helper/post_data_layanan');

const port = 8003;

describe('/api/v1?fn=findJadwalDokter', ()=>{
    const fn = 'findJadwalDokter'
    const data = {
        uid : "W77Xc_Til6cpmjUxmjd74Rth4rbd5EW-U-4iKIHHUXU"
    }
    it('status code harus 200', async(done)=>{
        const token = await signIn();
        
        const res = await post_layanan(port, fn, data, token);
        const status = res.status;
        // console.log(res.data);
        expect(status).toBe(200);
        done();
    })

    it('Memiliki data yg di return', async(done)=>{
        const token = await signIn();
        const res = await post_layanan(port, fn, data, token);
        const d = res.data;
        let empty = true;
        if(d.data) empty = false;
        expect(empty).toBeFalsy();
        done()
    })

    it('Data bukan dalam bentuk array', async(done)=>{
        const token = await signIn();
        const res = await post_layanan(port, fn, data, token);
        const d = res.data.data;
        let isArray = true;
        if(d.length){
            if(Array.isArray(d)===false) isArray = false;
        }
        else{
            isArray = false;
        }
        expect(isArray).toBeFalsy();
        done()
    })

    it('Harus ada data yang dikirim', async(done)=>{
        const token = await signIn();
        let d = {};
        const res = await post_layanan(port, fn, d, token);
        // console.log(res.data)
        const status = res.status;
        expect(status).toBe(200);
        done()
    })
})

const signIn = () =>{
    return new Promise( async (resolve, reject)=>{
        const user = {
            username : "senang",
            password : "senang1903"
        };
    
        const fn = 'signIn';
        const res = await post_data(fn, user)
        resolve(res.data.data.token)
    })
}