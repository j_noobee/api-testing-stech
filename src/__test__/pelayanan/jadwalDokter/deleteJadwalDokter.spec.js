const 
    post_data = require('../../../helper/post_data'),
    post_layanan = require('../../../helper/post_data_layanan');

const port = 8003;
describe('/api/v1?fn=deleteJadwalDokter', () => {
    

    

    it('status code harus 200', async(done)=>{
        // add data
        // const res = await post_layanan(port, fn, data, token);
        // console.log(add.data);
        const res = await deleteJadwalDokter();
        console.log(res);
        done();
    })
});

const createJadwalDokter = () =>{
    return new Promise(async(resolve, reject)=>{
        const data = {
            sip : "xxmmxx",
            uid_poli: "xx99",
            hari: "kamis",
            jam_mulai: "08:00",
            jam_selesai: "09:00"
        };
        const fn = "createJadwalDokter";
        const token = await signIn();
        const res = await post_layanan(port, fn, data, token);
        const d = {
            token,
            data : res.data
        }
        resolve(d)
    })
}

const deleteJadwalDokter = () =>{
    const fn = 'deleteJadwalDokter';
    return new Promise(async(resolve,reject)=>{
        const res = await createJadwalDokter();
        const data = {
            uid : res.data.data.uid
        }
        const delete = await post_layanan(port, fn, data, res.token );
        resolve(res);

    })
}

const signIn = () =>{
    return new Promise( async (resolve, reject)=>{
        const user = {
            username : "senang",
            password : "senang1903"
        };
    
        const fn = 'signIn';
        const res = await post_data(fn, user)
        resolve(res.data.data.token)
    })
}